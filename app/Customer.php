<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "customer";
    protected $fillable = ['id','firstname','lastname','idnumber','dateofbirth','phone','address','occupation','employername','employerphonenumber','email','password'];
}
