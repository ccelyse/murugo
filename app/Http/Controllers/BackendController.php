<?php

namespace App\Http\Controllers;

use AfricasTalking\SDK\AfricasTalking;
use App\Attractions;
use App\CompanyCategory;
use App\Countries;
use App\Customer;
use App\Developer;
use App\Hireaguide;
use App\JoinMember;
use App\LoanApplication;
use App\MortgageTracker;
use App\Post;
use App\Property;
use App\PropertyGallery;
use App\PropertyRange;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use function Sodium\add;

require_once('./Class_MR_SMS_API.php');

class BackendController extends Controller
{

    public function SignIn(){
        return view('backend.SignIn');
    }
    public function SignIn_(Request $request){
        $all = $request->all();
//        dd($all);
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
            $user_get_role = User::where('email', $request['email'])->value('role');
            switch ($user_get_role) {
                case "Admin":
                    return redirect()->intended('/Dashboard');
                    break;
                case "Credit Score":
                    return redirect()->intended('/CDashboard');
                    break;
                case "Financial Institution":
                    return redirect()->intended('/FDashboard');
                    break;
                default:
                    return back();
                    break;
            }
        }
        else{
            return back()->with('success','your email or your password is not matching');
        }
    }

    public function AccountList(){
        $listaccount = User::all();
        return view('backend.AccountList')->with(['listaccount'=>$listaccount]);
    }
    public function DeleteAccount(Request $request){
        $id = $request['id'];
        $delete = User::find($id);
        $delete->delete();
        return back()->with('success','You have successfully deleted an account');
    }
    public function EditAccount(Request $request){
        $all = $request->all();
        dd($all);
    }
    public function Dashboard(){
        $firstmonthday = new Carbon('first day of this month');
        $firstmonthlastday = new Carbon('last day of this month');
        $firstmonthname = $firstmonthday->format('F');

        $secondmonthday = new Carbon('first day of next month');
        $secondmonthlastday = new Carbon('last day of next month');
        $secondmonthname = $secondmonthday->format('F');

        $thirdmonthday = (new Carbon('first day of next month'))->addMonths(1);
        $thirdmonthlastday = $thirdmonthday->addWeeks(4);
        $thirdmonthname = $thirdmonthday->format('F');

        $developers = Developer::select(DB::raw('count(id) as devNumbers'))->value('devNumbers');
        $proNumbers = Property::select(DB::raw('count(id) as proNumbers'))->value('proNumbers');
        $AppUser = Customer::select(DB::raw('count(id) as userNumbers'))->value('userNumbers');
        $LoanNumber = LoanApplication::select(DB::raw('count(id) as LoanNumber'))->value('LoanNumber');

        $numberofusersfirst = Customer::select(DB::raw('count(id) as numberUsersFirst'))
            ->whereBetween('created_at',[$firstmonthday->format('Y/m/d'),$firstmonthlastday->format('Y/m/d')])
            ->value('numberUsersFirst');

        $numberofuserssecond = Customer::select(DB::raw('count(id) as numberUsersSecond'))
            ->whereBetween('created_at',[$secondmonthday->format('Y/m/d'),$secondmonthlastday->format('Y/m/d')])
            ->value('numberUsersFirst');

        $numberofusersthird = Customer::select(DB::raw('count(id) as numberUsersThird'))
            ->whereBetween('created_at',[$thirdmonthday->format('Y/m/d'),$thirdmonthlastday->format('Y/m/d')])
            ->value('numberUsersFirst');

//        dd($numberofusersthird);
        return view('backend.Dashboard')->with(['numberofusersthird'=>$numberofusersthird,'numberofuserssecond'=>$numberofuserssecond,'numberofusersfirst'=>$numberofusersfirst,'firstmonthname'=>$firstmonthname,'secondmonthname'=>$secondmonthname,'thirdmonthname'=>$thirdmonthname,'developers'=>$developers,'AppUser'=>$AppUser,'proNumbers'=>$proNumbers,'LoanNumber'=>$LoanNumber]);
    }

    public function CDashboard(){
        $firstmonthday = new Carbon('first day of this month');
        $firstmonthlastday = new Carbon('last day of this month');
        $firstmonthname = $firstmonthday->format('F');

        $secondmonthday = new Carbon('first day of next month');
        $secondmonthlastday = new Carbon('last day of next month');
        $secondmonthname = $secondmonthday->format('F');

        $thirdmonthday = (new Carbon('first day of next month'))->addMonths(1);
        $thirdmonthlastday = $thirdmonthday->addWeeks(4);
        $thirdmonthname = $thirdmonthday->format('F');

        $developers = Developer::select(DB::raw('count(id) as devNumbers'))->value('devNumbers');
        $proNumbers = Property::select(DB::raw('count(id) as proNumbers'))->value('proNumbers');
        $AppUser = Customer::select(DB::raw('count(id) as userNumbers'))->value('userNumbers');
        $LoanNumber = LoanApplication::select(DB::raw('count(id) as LoanNumber'))->value('LoanNumber');

        $numberofusersfirst = Customer::select(DB::raw('count(id) as numberUsersFirst'))
            ->whereBetween('created_at',[$firstmonthday->format('Y/m/d'),$firstmonthlastday->format('Y/m/d')])
            ->value('numberUsersFirst');

        $numberofuserssecond = Customer::select(DB::raw('count(id) as numberUsersSecond'))
            ->whereBetween('created_at',[$secondmonthday->format('Y/m/d'),$secondmonthlastday->format('Y/m/d')])
            ->value('numberUsersFirst');

        $numberofusersthird = Customer::select(DB::raw('count(id) as numberUsersThird'))
            ->whereBetween('created_at',[$thirdmonthday->format('Y/m/d'),$thirdmonthlastday->format('Y/m/d')])
            ->value('numberUsersFirst');

//        dd($numberofusersthird);
        return view('backend.Dashboard')->with(['numberofusersthird'=>$numberofusersthird,'numberofuserssecond'=>$numberofuserssecond,'numberofusersfirst'=>$numberofusersfirst,'firstmonthname'=>$firstmonthname,'secondmonthname'=>$secondmonthname,'thirdmonthname'=>$thirdmonthname,'developers'=>$developers,'AppUser'=>$AppUser,'proNumbers'=>$proNumbers,'LoanNumber'=>$LoanNumber]);
    }

    public function FDashboard(){
        $firstmonthday = new Carbon('first day of this month');
        $firstmonthlastday = new Carbon('last day of this month');
        $firstmonthname = $firstmonthday->format('F');

        $secondmonthday = new Carbon('first day of next month');
        $secondmonthlastday = new Carbon('last day of next month');
        $secondmonthname = $secondmonthday->format('F');

        $thirdmonthday = (new Carbon('first day of next month'))->addMonths(1);
        $thirdmonthlastday = $thirdmonthday->addWeeks(4);
        $thirdmonthname = $thirdmonthday->format('F');

        $developers = Developer::select(DB::raw('count(id) as devNumbers'))->value('devNumbers');
        $proNumbers = Property::select(DB::raw('count(id) as proNumbers'))->value('proNumbers');
        $AppUser = Customer::select(DB::raw('count(id) as userNumbers'))->value('userNumbers');
        $LoanNumber = LoanApplication::select(DB::raw('count(id) as LoanNumber'))->value('LoanNumber');

        $numberofusersfirst = Customer::select(DB::raw('count(id) as numberUsersFirst'))
            ->whereBetween('created_at',[$firstmonthday->format('Y/m/d'),$firstmonthlastday->format('Y/m/d')])
            ->value('numberUsersFirst');

        $numberofuserssecond = Customer::select(DB::raw('count(id) as numberUsersSecond'))
            ->whereBetween('created_at',[$secondmonthday->format('Y/m/d'),$secondmonthlastday->format('Y/m/d')])
            ->value('numberUsersFirst');

        $numberofusersthird = Customer::select(DB::raw('count(id) as numberUsersThird'))
            ->whereBetween('created_at',[$thirdmonthday->format('Y/m/d'),$thirdmonthlastday->format('Y/m/d')])
            ->value('numberUsersFirst');

//        dd($numberofusersthird);
        return view('backend.Dashboard')->with(['numberofusersthird'=>$numberofusersthird,'numberofuserssecond'=>$numberofuserssecond,'numberofusersfirst'=>$numberofusersfirst,'firstmonthname'=>$firstmonthname,'secondmonthname'=>$secondmonthname,'thirdmonthname'=>$thirdmonthname,'developers'=>$developers,'AppUser'=>$AppUser,'proNumbers'=>$proNumbers,'LoanNumber'=>$LoanNumber]);
    }

    public function CreateAccount(){
        return view('backend.CreateAccount');
    }

    public function CreateAccount_(Request $request){
        $all  = $request->all();

        $request->validate([
            'name' => 'required|string:posts|max:255',
            'email' => 'required|string|email|max:255|unique:users','exists:connection.user,email',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $register = new User();
        $register->name = $request['name'];
        $register->email = $request['email'];
        $register->role = $request['user_role'];
        $register->password = bcrypt($request['password']);
        $register->save();

        return redirect()->route('backend.AccountList')->with('Success','You have successfully Created Your Account');
    }

    public function Developer(){
        $listhouses = Developer::all();
        return view('backend.developer')->with(['listhouses'=>$listhouses]);
    }
    public function AddDeveloper(Request $request){
        $all = $request->all();
        $adddev = new Developer();
        $adddev->developer_name = $request['developer_name'];
        $adddev->developer_phonenumber = $request['developer_phonenumber'];
        $adddev->developer_address = $request['developer_address'];
        $adddev->developer_email = $request['developer_email'];
        $adddev->save();
        return back()->with('success','you have successfully added a developer');
    }

    public function EditDeveloper(Request $request){
        $id = $request['id'];
        $adddev = Developer::find($id);
        $adddev->developer_name = $request['developer_name'];
        $adddev->developer_phonenumber = $request['developer_phonenumber'];
        $adddev->developer_address = $request['developer_address'];
        $adddev->developer_email = $request['developer_email'];
        $adddev->save();
        return back()->with('success','you have successfully edited a developer information');
    }
    public function DeleteDeveloper(Request $request){
        $id = $request['id'];
        $adddev = Developer::find($id);
        $adddev->delete();
        return back()->with('success','you have successfully deleted a developer information');
    }
    public function PropertyRangeB(){
        $listrange = PropertyRange::all();
        return view('backend.PropertyRange')->with(['listrange'=>$listrange]);
    }
    public function AddPropertyRangeB(Request $request){
        $all = $request->all();
        $addrange = new PropertyRange();
        $addrange->property_mortgagerange = $request['property_mortgagerange'];
        $addrange->save();
        return back()->with('success','you have successfully added property range');
    }
    public function UpdatePropertyRangeB(Request $request){
        $id = $request['id'];
        $addrange = PropertyRange::find($id);
        $addrange->property_mortgagerange = $request['property_mortgagerange'];
        $addrange->save();
        return back()->with('success','you have successfully updated property range');
    }
    public function DeletePropertyRangeB(Request $request){
        $id = $request['id'];
        $addrange = PropertyRange::find($id);
        $addrange->delete();
        return back()->with('success','you have successfully deleted property range');
    }
    public function PropertyRangeApi(Request $request){
        $listrange = PropertyRange::all();
        return response()->json($listrange);
    }
    public function Property(){
        $listdev = Developer::all();
        $listhouses = Property::all();
        $listrange = PropertyRange::all();
        return view('backend.Property')->with(['listrange'=>$listrange,'listhouses'=>$listhouses,'listdev'=>$listdev]);
    }
    public function DeleteProperty(Request $request){
        $id = $request['id'];
        $delete = Property::find($id);
        $delete->delete();
        return back()->with('success','you have successfully deleted property');
    }
    public function AddProperty(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;

        $addhouses = new Property();
        $addhouses ->property_location = $request['property_location'];
        $addhouses ->property_name = $request['property_name'];
        $addhouses ->property_price = $request['property_price'];
        $addhouses ->bathrooms = $request['bathrooms'];
        $addhouses ->bedrooms = $request['bedrooms'];
        $addhouses ->plotsize = $request['plotsize'];
        $addhouses ->property_status = $request['property_status'];
        $addhouses ->property_mortgagerange = $request['property_mortgagerange'];
        $addhouses->property_indetails = $request['property_indetails'];
        $addhouses->property_developer = $request['property_developer'];

        $image = $request->file('property_image');
        $addhouses->property_image = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/PropertyImages');
        $image->move($destinationPath, $imagename);

        $addhouses->save();

        $previous_id = $addhouses->id;

        $property_image = $request['propertygallery'];

        foreach ($property_image as $property_images) {
            $image = $request->file('propertygallery');
            $addpicture = new PropertyGallery();
            $addpicture->property_id = $previous_id;
            $addpicture->property_pic_name = $property_images->getClientOriginalName();
            $imagename = $property_images->getClientOriginalName();
            $destinationPath = public_path('/PropertyImages');
            $property_images->move($destinationPath, $imagename);
            $addpicture->save();
        }
        return back()->with('success','you have successfully added a new property');
    }

    public function EditProperty(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;

        if($request->file('property_image')){
            $addhouses = Property::find($user_id);
            $addhouses ->property_location = $request['property_location'];
            $addhouses ->property_name = $request['property_name'];
            $addhouses ->property_price = $request['property_price'];
            $addhouses ->bathrooms = $request['bathrooms'];
            $addhouses ->bedrooms = $request['bedrooms'];
            $addhouses ->plotsize = $request['plotsize'];
            $addhouses ->property_status = $request['property_status'];
            $addhouses ->property_mortgagerange = $request['property_mortgagerange'];
            $addhouses->property_indetails = $request['property_indetails'];
            $image = $request->file('property_image');
            $addhouses->property_image = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/PropertyImages');
            $image->move($destinationPath, $imagename);

            $addhouses->save();

            return back()->with('success','you have successfully edited a new property');

        }else{
            $addhouses = Property::find($user_id);
            $addhouses ->property_location = $request['property_location'];
            $addhouses ->property_name = $request['property_name'];
            $addhouses ->property_price = $request['property_price'];
            $addhouses ->bathrooms = $request['bathrooms'];
            $addhouses ->bedrooms = $request['bedrooms'];
            $addhouses ->plotsize = $request['plotsize'];
            $addhouses ->property_status = $request['property_status'];
            $addhouses ->property_mortgagerange = $request['property_mortgagerange'];
            $addhouses->property_indetails = $request['property_indetails'];
            $addhouses->save();

            return back()->with('success','you have successfully edited  property');
        }

    }
    public function PropertyRange(Request $request){
        $range = $request['range'];
        $listhouses = Property::where('property_mortgagerange',$range)->get();
        return response()->json($listhouses);
    }
    public function PropertyMobile(){
        $listhouses = Property::all();
        return response()->json($listhouses);
    }
    public function PropertyMore(Request $request){
        $id= $request['id'];
        $listhouses = Property::where('id',$id)->get();
        return response()->json($listhouses);
    }
    public function PropertyGallery(Request $request){
        $id= $request['id'];
        $listhouses = PropertyGallery::where('property_id',$id)->get();
        return response()->json($listhouses);
    }


    public function SendSMS_(){

        $rsgamessage = "Murugo testing";
        $phonenumber = '0782384772';

        $intnumber = '25'.$phonenumber;
        $api_key = 'ZXFnS0U9cmZ6aFBGZXlnPXVEUGw=';
        $from = 'MURUGO';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();

        $response = $client->send_sms($sms_body, $url);

        $response=json_decode($response);

        dd($response);

//        return redirect()->route('backend.SendSMS')->with('success','you have successfully sent your message');
    }
    public function SendEmail(){

    }

    public function CustomerSignUp(Request $request){
        $addcustomer = new Customer();
        $addcustomer->firstname = $request['firstname'];
        $addcustomer->lastname = $request['lastname'];
        $addcustomer->idnumber = $request['idnumber'];
        $addcustomer->dateofbirth = $request['dateofbirth'];
        $addcustomer->phone = $request['phone'];
        $addcustomer->address = $request['address'];
        $addcustomer->occupation = $request['occupation'];
        $addcustomer->employername = $request['employername'];
        $addcustomer->employerphonenumber = $request['employerphonenumber'];
        $addcustomer->email = $request['email'];
        $addcustomer->password = bcrypt($request['password']);
        $addcustomer->save();
        return response()->json(['status'=>'200']);
    }
    public function Customers(){
        $listcustomers = Customer::all();
        return view ('backend.Customers')->with(['listcustomers'=>$listcustomers]);

    }
    public function CustomerSignIn(Request $request){
        $userdata = array(
            'email' => $request['email'] ,
            'password' => $request['password']
        );
        $checkaccount = Customer::where('email',$request['email'])->value('password');
        if(Hash::check($request['password'],$checkaccount)) {
            $userdatas = Customer::where('email',$request['email'])->get();
            foreach ($userdatas as $userdatass){
                $firstname = $userdatass->firstname;
                $lastname = $userdatass->lastname;
                $user_id = $userdatass->id;
            }
            return response()->json(['status_code'=>'200','message'=>'success','firstname'=>$firstname,'lastname'=>$lastname,'user_id'=>$user_id]);

        } else {
            return response()->json(['status_code'=>'201','message'=>'Ooops, account not found']);
        }


    }
    public function CustomerDetails(Request $request){

        $userdatas = Customer::where('email',$request['email'])->get();
        foreach ($userdatas as $userdatass){
            $firstname = $userdatass->firstname;
            $lastname = $userdatass->lastname;
            $user_id = $userdatass->id;
        }

        return response()->json(['status_code'=>'200','message'=>'success','firstname'=>$firstname,'lastname'=>$lastname,'user_id'=>$user_id]);
    }
    public function ApplyMortgage(Request $request){
        $addmort = new LoanApplication();
        $addmort->customer_id = $request['user_id'];
        $addmort->property_id = $request['property_id'];
        $addmort->credit_score_status = 'Pending';
        $addmort->financial_status = 'Pending';
        $addmort->save();

        $lastid = $addmort->id;

        $add_status = new MortgageTracker();
        $add_status->user_id = $request['user_id'];
        $add_status->mortgagestatus = "Successfully Applied";
        $add_status->mortgagelevel = "Mortgage Application";
        $add_status->application_id = $lastid;
        $add_status->save();

        $add_status = new MortgageTracker();
        $add_status->user_id = $request['user_id'];
        $add_status->mortgagestatus = "Pending";
        $add_status->mortgagelevel = "Credit Score";
        $add_status->application_id = $lastid;
        $add_status->save();

        $add_status = new MortgageTracker();
        $add_status->user_id = $request['user_id'];
        $add_status->mortgagestatus = "Pending";
        $add_status->mortgagelevel = "Financial Institution";
        $add_status->application_id =$lastid;
        $add_status->save();

        $getcustomerid = LoanApplication::where('id',$request['user_id'])->value('customer_id');
        $getcustomerphone = Customer::where('id',$getcustomerid)->value('phone');
        $getcustomernames = Customer::where('id',$getcustomerid)->value('firstname');
        $getcustomeremail = Customer::where('id',$getcustomerid)->value('email');

        $rsgamessage = "Dear $getcustomernames \n Thank you for applying for affordable mortgage your application have been sent to review you will be communicated once it is approved or rejected";
        $phonenumber = '0782384772';

        $intnumber = '25'.$getcustomerphone;
        $api_key = 'ZXFnS0U9cmZ6aFBGZXlnPXVEUGw=';
        $from = 'MURUGO';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();
        $response = $client->send_sms($sms_body, $url);

        $name = $getcustomernames;
        $email = "murugoapp@gmail.com";
        $toemail = $getcustomeremail;
        $messageMail = "Thank you for applying for affordable mortgage your application have been sent to review you will be communicated once it is approved or rejected";
        $company_name = "MURUGO APP";
        $subjectmail = "Mortgage Application";

        Mail::send('backend.mail',
            array(
                'namemail'=>$company_name,
                'name' => $name,
                'email' => $email,
                'messageMail' => $messageMail
            ), function($message) use ($email,$messageMail, $name,$subjectmail,$company_name,$toemail)
            {
                $message->from($email, $company_name);
                $message->to($toemail, $name)->subject($subjectmail);
            });
//        $response =json_decode($response);
        return response()->json(['status_code'=>'200','message'=>'You have successfully applied for a mortgage']);
    }
    public function TestEmail(){
//

        $name = "Elysee CONFIANCE";
        $email = "murugoapp@gmail.com";
        $toemail = "ccelyse1@gmail.com";
        $messageMail = "Thank you for applying for affordable mortgage your application have been sent to review you will be communicated once it is approved or rejected";
        $company_name = "MURUGO APP";
        $subjectmail = "Mortgage Application";

        Mail::send('backend.mail',
            array(
                'namemail'=>$company_name,
                'name' => $name,
                'email' => $email,
                'messageMail' => $messageMail
            ), function($message) use ($email,$messageMail, $name,$subjectmail,$company_name,$toemail)
            {
                $message->from($email, $company_name);
                $message->to($toemail, $name)->subject($subjectmail);
            });
    }

    public function ApplicationList(){
        $listdev = Developer::all();
        $listhouses = Property::all();
        $listapp = LoanApplication::join('customer', 'customer.id', '=', 'loanapplication.customer_id')
            ->join('properties', 'properties.id', '=', 'loanapplication.property_id')
            ->select('loanapplication.id','customer.firstname','customer.lastname', 'customer.lastname','customer.phone','customer.email','customer.occupation','customer.employername','properties.property_price','properties.property_status','properties.property_mortgagerange','properties.property_image')
            ->get();
        return view('backend.ApplicationList')->with(['listapp'=>$listapp]);
    }

    public function ApproveSendSMS(Request $request){
        $id = $request['id'];
        $getcustomerid = LoanApplication::where('id',$id)->value('customer_id');
        $getcustomerphone = Customer::where('id',$getcustomerid)->value('phone');
        $getcustomernames = Customer::where('id',$getcustomerid)->value('firstname');
        $getcustomeremail = Customer::where('id',$getcustomerid)->value('email');
        $updatetracker = MortgageTracker::where('application_id',$id)->where('mortgagelevel','Financial Institution')->update(['mortgagestatus'=>'Your application for mortgage  have been approved']);

        $updatecredit = LoanApplication::where('id',$id)->update(['financial_status' => 'Approved']);
        $rsgamessage = "Dear $getcustomernames\nYour application for mortgage have been approved ,kindly visit the BRD office to finalise the  application";
        $phonenumber = '0782384772';

        $intnumber = '25'.$getcustomerphone;
        $api_key = 'ZXFnS0U9cmZ6aFBGZXlnPXVEUGw=';
        $from = 'MURUGO';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();
        $response = $client->send_sms($sms_body, $url);
        $response =json_decode($response);

        $name = $getcustomernames;
        $email = "murugoapp@gmail.com";
        $toemail = $getcustomeremail;
        $messageMail = "Your application for mortgage have been approved ,kindly visit the BRD office to finalise the  application";
        $company_name = "MURUGO APP";
        $subjectmail = "Mortgage Application";

        Mail::send('backend.mail',
            array(
                'namemail'=>$company_name,
                'name' => $name,
                'email' => $email,
                'messageMail' => $messageMail
            ), function($message) use ($email,$messageMail, $name,$subjectmail,$company_name,$toemail)
            {
                $message->from($email, $company_name);
                $message->to($toemail, $name)->subject($subjectmail);
            });


        return back()->with('success','you have successfully sent your message');

    }

    public function RejectSendSMS(Request $request){
        $id = $request['id'];
        $getcustomerid = LoanApplication::where('id',$id)->value('customer_id');
        $getcustomerphone = Customer::where('id',$getcustomerid)->value('phone');
        $getcustomernames = Customer::where('id',$getcustomerid)->value('firstname');
        $updatecredit = LoanApplication::where('id',$id)->update(['financial_status' => 'Rejected']);
        $getcustomeremail = Customer::where('id',$getcustomerid)->value('email');

        $updatetracker = MortgageTracker::where('application_id',$id)->where('mortgagelevel','Financial Institution')->update(['mortgagestatus'=>'Your application for mortgage  have been rejected']);

        $rsgamessage = "Dear $getcustomernames\nwe  sorry  to say that  your application for mortgage  have been rejected";
        $phonenumber = '0782384772';

        $intnumber = '25'.$getcustomerphone;
        $api_key = 'ZXFnS0U9cmZ6aFBGZXlnPXVEUGw=';
        $from = 'MURUGO';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();
        $response = $client->send_sms($sms_body, $url);
        $response =json_decode($response);

        $name = $getcustomernames;
        $email = "murugoapp@gmail.com";
        $toemail = $getcustomeremail;
        $messageMail = "We  sorry to say that your application for mortgage have been rejected";
        $company_name = "MURUGO APP";
        $subjectmail = "Mortgage Application";

        Mail::send('backend.mail',
            array(
                'namemail'=>$company_name,
                'name' => $name,
                'email' => $email,
                'messageMail' => $messageMail
            ), function($message) use ($email,$messageMail, $name,$subjectmail,$company_name,$toemail)
            {
                $message->from($email, $company_name);
                $message->to($toemail, $name)->subject($subjectmail);
            });

        return back()->with('success','you have successfully sent your message');
    }
    public function CreditScore(){
        $listdev = Developer::all();
        $listhouses = Property::all();
        $listapp = LoanApplication::join('customer', 'customer.id', '=', 'loanapplication.customer_id')
            ->join('properties', 'properties.id', '=', 'loanapplication.property_id')
            ->select('loanapplication.id','loanapplication.credit_score_comment','customer.firstname','customer.lastname', 'customer.lastname','customer.phone','customer.email','customer.occupation','customer.employername','properties.property_price','properties.property_status','properties.property_mortgagerange','properties.property_image')
            ->get();
        return view('backend.CreditScore')->with(['listapp'=>$listapp]);
    }
    public function CreditScoreComment(Request $request){
        $id = $request['id'];
        $updatetracker = MortgageTracker::where('application_id',$id)->where('mortgagelevel','Credit Score')->update(['mortgagestatus'=>'Your application has been checked']);
        $updatecredit = LoanApplication::where('id',$id)->update(['credit_score_comment' => $request['credit_score_comment'],'credit_score_status'=>'Checked']);
        return back()->with('success','you have successfully update credit score');
    }
    public function FinancialInstitution(){
        $listdev = Developer::all();
        $listhouses = Property::all();
        $listapp = LoanApplication::where('loanapplication.credit_score_status','Checked')->join('customer', 'customer.id', '=', 'loanapplication.customer_id')
            ->join('properties', 'properties.id', '=', 'loanapplication.property_id')
            ->select('loanapplication.id','loanapplication.financial_status','loanapplication.credit_score_comment','customer.firstname','customer.lastname', 'customer.lastname','customer.phone','customer.email','customer.occupation','customer.employername','properties.property_price','properties.property_status','properties.property_mortgagerange','properties.property_image')
            ->get();
        return view('backend.FinancialInstitution')->with(['listapp'=>$listapp]);
    }
    public function CustomerInformation(Request $request){
        $userdatas = Customer::where('email',$request['email'])->get();
//        $userdatas = Customer::where('email','kalisa913@gmail.com')->get();
        foreach ($userdatas as $userdatass){
            $firstname = $userdatass->firstname;
            $lastname = $userdatass->lastname;
            $idnumber = $userdatass->idnumber;
            $dateofbirth = $userdatass->dateofbirth;
            $phone = $userdatass->phone;
            $address = $userdatass->address;
            $occupation = $userdatass->occupation;
            $employername = $userdatass->employername;
            $employerphonenumber = $userdatass->employerphonenumber;
            $email = $userdatass->email;
            $user_id = $userdatass->id;
        }
        return response()->json(['status_code'=>'200',
            'message'=>'success',
            'firstname'=>$firstname,
            'lastname'=>$lastname,
            'idnumber'=>$idnumber,
            'dateofbirth'=>$dateofbirth,
            'phone'=>$phone,
            'address'=>$address,
            'occupation'=>$occupation,
            'employername'=>$employername,
            'employerphonenumber'=>$employerphonenumber,
            'email'=>$email,
            'user_id'=>$user_id
        ]);
    }
    public function UpdateCustomerInformation(Request $request){
        $id = $request['UserCustomerId'];
        $addcustomer = Customer::find($id);
        $addcustomer->firstname = $request['firstname'];
        $addcustomer->lastname = $request['lastname'];
        $addcustomer->idnumber = $request['idnumber'];
        $addcustomer->dateofbirth = $request['dateofbirth'];
        $addcustomer->phone = $request['phone'];
        $addcustomer->address = $request['address'];
        $addcustomer->occupation = $request['occupation'];
        $addcustomer->employername = $request['employername'];
        $addcustomer->employerphonenumber = $request['employerphonenumber'];
        $addcustomer->email = $request['email'];
        $addcustomer->save();
        return response()->json(['status'=>'200']);
    }
    public function LevelTracker(Request $request){
        $email = $request['email'];
//        $email = "kalisa913@gmail.com";
        $getid = Customer::where('email',$email)->value('id');
        $timenow = Carbon::now();
        $onlydate = $timenow->toDateString();
        $checkstatus = MortgageTracker::where('user_id',$getid)->where('mortgagelevel','Mortgage Application')->get();

        if(0 == count($checkstatus)){
            return response()->json(['status_code'=>'200',
                'message'=>'success',
                'time'=>$onlydate,
                'mortgagestatus'=>"No status yet",
            ]);
        }else{
            foreach ($checkstatus as $datas){
                $time = $datas->created_at;
                $mortgagestatus = $datas->mortgagestatus;
            }
            return response()->json(['status_code'=>'200',
                'message'=>'success',
                'time'=>$time,
                'mortgagestatus'=>$mortgagestatus,
            ]);
        }


    }
    public function LevelOneTracker(Request $request){
        $email = $request['email'];
//        $email = "kalisa913@gmail.com";
        $getid = Customer::where('email',$email)->value('id');
        $timenow = Carbon::now();
        $onlydate = $timenow->toDateString();
        $checkstatus = MortgageTracker::where('user_id',$getid)->where('mortgagelevel','Credit Score')->get();

        if(0 == count($checkstatus)){
            return response()->json(['status_code'=>'200',
                'message'=>'success',
                'time'=>$onlydate,
                'mortgagestatus'=>"No status yet",
            ]);
        }else{
            foreach ($checkstatus as $datas){
                $time = $datas->created_at;
                $mortgagestatus = $datas->mortgagestatus;
            }
            return response()->json(['status_code'=>'200',
                'message'=>'success',
                'time'=>$time,
                'mortgagestatus'=>$mortgagestatus,
            ]);
        }


    }
    public function LevelTwoTracker(Request $request){
        $email = $request['email'];
        $getid = Customer::where('email',$email)->value('id');
        $timenow = Carbon::now();
        $onlydate = $timenow->toDateString();
        $checkstatus = MortgageTracker::where('user_id',$getid)->where('mortgagelevel','Financial Institution')->get();
        if(0 == count($checkstatus)){
            return response()->json(['status_code'=>'200',
                'message'=>'success',
                'time'=>$onlydate,
                'mortgagestatus'=>"No status yet",
            ]);
        }else{
            foreach ($checkstatus as $datas){
                $time = $datas->created_at;
                $mortgagestatus = $datas->mortgagestatus;
            }
            return response()->json(['status_code'=>'200',
                'message'=>'success',
                'time'=>$time,
                'mortgagestatus'=>$mortgagestatus,
            ]);
        }
    }

    }
