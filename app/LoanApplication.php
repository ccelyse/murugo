<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanApplication extends Model
{
    protected $table = "loanapplication";
    protected $fillable = ['id','customer_id','property_id','credit_score_comment','credit_score_status','financial_status'];
}
