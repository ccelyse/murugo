<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MortgageTracker extends Model
{
    protected $table = "mortgagetracker";
    protected $fillable = ['id','mortgagestatus','mortgagelevel','application_id','user_id'];
}
