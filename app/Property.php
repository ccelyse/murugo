<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = "properties";
    protected $fillable = ['id','property_location','property_name','property_image','property_indetails','property_developer'];
}
