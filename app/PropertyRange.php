<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyRange extends Model
{
    protected $table = "propertyrange";
    protected $fillable = ['id','property_mortgagerange'];
}
