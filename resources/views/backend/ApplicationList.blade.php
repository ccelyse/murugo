@extends('backend.layout.master')

@section('title', 'Affordable Housing')

@section('content')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')


    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->

                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Occupation</th>
                                                    <th>Phone Number</th>
                                                    <th>Email</th>
                                                    <th>Employer Name</th>
                                                    <th>Property Price</th>
                                                    <th>Property Status</th>
                                                    <th>Property Mortgage fee range</th>
                                                    <th>Property Cover Picture</th>
                                                    <th>Approve</th>
                                                    <th>Approve</th>
                                                    <th>Reject</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listapp as $data)
                                                    <tr>
                                                        <td>{{$data->firstname}}</td>
                                                        <td>{{$data->lastname}}</td>
                                                        <td>{{$data->occupation}}</td>
                                                        <td>{{$data->phone}}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td>{{$data->employername}}</td>
                                                        <td>{{$data->property_price}}</td>
                                                        <td>{{$data->property_status}}</td>
                                                        <td>{{$data->property_mortgagerange}}</td>
                                                        <td><img src="PropertyImages/{{$data->property_image}}" style="width:100%"></td>
                                                        <td>{{$data->property_mortgagerange}}</td>
                                                        <td><a href="{{ route('backend.ApproveSendSMS',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Approve</a></td>
                                                        <td><a href="{{ route('backend.RejectSendSMS',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Reject</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Summernote Click to edit end -->
                    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
                    {{--<script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>--}}
                    {{--<script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>--}}

                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
                            type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
