@extends('backend.layout.master')

@section('title', 'Affordable Housing')

@section('content')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')


    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->


                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>ID number</th>
                                                    <th>Date of Birth</th>
                                                    <th>Phone Number</th>
                                                    <th>Address</th>
                                                    <th>Occupation</th>
                                                    <th>Employer Name</th>
                                                    <th>Employer Phone Number</th>
                                                    <th>Email</th>
                                                    <th>Date Created</th>
                                                    <th>Delete</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listcustomers as $data)
                                                    <tr>
                                                        <td>{{$data->firstname}}</td>
                                                        <td>{{$data->lastname}}</td>
                                                        <td>{{$data->idnumber}}</td>
                                                        <td>{{$data->dateofbirth}}</td>
                                                        <td>{{$data->phone}}</td>
                                                        <td>{{$data->address}}</td>
                                                        <td>{{$data->occupation}}</td>
                                                        <td>{{$data->employername}}</td>
                                                        <td>{{$data->employerphonenumber}}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td><a href="{{ route('backend.DeleteProperty',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
                            type="text/javascript"></script>
                    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
                    {{--<script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>--}}
                    {{--<script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>--}}
                </div>
            </div>
        </div>
    </div>

@endsection
