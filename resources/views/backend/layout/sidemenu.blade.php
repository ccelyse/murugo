<style>
    .main-menu.menu-light .navigation>li.open>a {
        color: #545766;
        background: #f5f5f5;
        border-right: 4px solid #6b442b !important;
    }
</style>
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <?php
            $user_id = \Auth::user()->id;
            $Userrole = \App\User::where('id',$user_id)->value('role');

            $Dashboard = url('Dashboard');
            $CDashboard = url('CDashboard');
            $FDashboard = url('FDashboard');
            $CreateAccount = url('CreateAccount');
            $AccountList = url('AccountList');
            $Developer = url('Developer');
            $PropertyRangeB = url('PropertyRangeB');
            $Property = url('Property');
            $ApplicationList = url('ApplicationList');
            $Customers = url('Customers');
            $CreditScore = url('CreditScore');
            $FinancialInstitution = url('FinancialInstitution');

            switch ($Userrole) {
                case "Admin":
                    echo "<li class=' nav-item'><a href='$Dashboard'><i class='fas fa-chart-line'></i><span class='menu-title' data-i18n='nav.dash.main'>Dashboard</span></a>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.templates.main'>Account</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$CreateAccount'><i class='fas fa-user-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Create Account</span></a> </li><li class=' nav-item'><a href='$AccountList'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Account List</span></a> </li></ul> </li>";
                    echo "<li class=' nav-item'><a href='$Developer'><i class='fas fa-building'></i><span class='menu-title' data-i18n='nav.dash.main'>Developer</span></a>";
                    echo "<li class=' nav-item'><a href='$PropertyRangeB'><i class='fas fa-money-bill-wave'></i><span class='menu-title' data-i18n='nav.dash.main'>Property Range</span></a>";
                    echo "<li class=' nav-item'><a href='$Property'><i class='fas fa-home'></i><span class='menu-title' data-i18n='nav.dash.main'>Properties</span></a>";
                    echo "<li class=' nav-item'><a href='$ApplicationList'><i class='fas fa-list-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Applications</span></a>";
                    echo "<li class=' nav-item'><a href='$Customers'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.dash.main'>Customers</span></a>";
                    break;

                case "Credit Score":
                    echo "<li class=' nav-item'><a href='$CDashboard'><i class='fas fa-chart-line'></i><span class='menu-title' data-i18n='nav.dash.main'>Dashboard</span></a>";
                    echo "<li class=' nav-item'><a href='$CreditScore'><i class='fas fa-coins'></i><span class='menu-title' data-i18n='nav.dash.main'>Credit Score</span></a>";

                    break;

                case "Financial Institution":
                    echo "<li class=' nav-item'><a href='$FDashboard'><i class='fas fa-chart-line'></i><span class='menu-title' data-i18n='nav.dash.main'>Dashboard</span></a>";
                    echo "<li class=' nav-item'><a href='$FinancialInstitution'><i class='fas fa-money-bill-wave'></i><span class='menu-title' data-i18n='nav.dash.main'>Financial Institution</span></a>";

                    break;

                default:
                    return redirect()->back();
                    break;
            }
            ?>

            {{--<li class=" nav-item"><a href="#"><i class="fas fa-user-circle"></i><span class="menu-title" data-i18n="nav.templates.main">Account</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('CreateAccount')}}"><i class="fas fa-user-alt"></i><span class="menu-title" data-i18n="nav.dash.main">Create Account</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('AccountList')}}"><i class="fas fa-user-circle"></i><span class="menu-title" data-i18n="nav.dash.main">Account List</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class=" nav-item"><a href="{{url('Developer')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Developer</span></a>--}}
            {{--<li class=" nav-item"><a href="{{url('PropertyRangeB')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Property Range</span></a>--}}
            {{--<li class=" nav-item"><a href="{{url('Property')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Properties</span></a>--}}
            {{--<li class=" nav-item"><a href="{{url('ApplicationList')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Applications</span></a>--}}
            {{--<li class=" nav-item"><a href="{{url('Customers')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Customers</span></a>--}}
            {{--<li class=" nav-item"><a href="{{url('CreditScore')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Credit Score</span></a>--}}
            {{--<li class=" nav-item"><a href="{{url('FinancialInstitution')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Financial Institution</span></a>--}}

        </ul>
    </div>
</div>


