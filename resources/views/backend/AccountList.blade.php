@extends('backend.layout.master')

@section('title', 'Murugo')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <!-- Complex headers with column visibility table -->

                <section id="setting">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Murugo Account List</h4>
                                    @if (session('Success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('Success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">

                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Created At</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listaccount as $data)
                                            <tr>
                                                <td>{{$data->name}}</td>
                                                <td>{{$data->email}}</td>
                                                <td>{{$data->role}}</td>
                                                <td>{{$data->created_at}}</td>
                                                <td>

                                                    <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#editplace{{$data->id}}">Edit
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="editplace{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Edit</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('EditAccount') }}">
                                                                        {{ csrf_field() }}
                                                                        <fieldset class="form-group position-relative has-icon-left mb-1">
                                                                            <input type="text" class="form-control form-control-lg input-lg" id="user-name" name="name" value="{{$data->name}}"  placeholder="names" autofocus>
                                                                            <input type="text" class="form-control form-control-lg input-lg" id="user-name" name="id" value="{{$data->id}}" hidden>

                                                                            <div class="form-control-position">
                                                                                <i class="ft-user"></i>
                                                                            </div>
                                                                        </fieldset>

                                                                        <fieldset class="form-group position-relative has-icon-left mb-1">
                                                                            <input type="email" class="form-control form-control-lg input-lg" id="user-email" name="email" name="email" value="{{$data->email}}"  placeholder="email" required>
                                                                            <div class="form-control-position">
                                                                                <i class="ft-mail"></i>
                                                                            </div>
                                                                        </fieldset>

                                                                        <fieldset class="form-group position-relative has-icon-left">
                                                                            <select class="form-control"  id="basicSelect" name="user_role" required>
                                                                                <option value="{{$data->role}}">{{$data->role}}</option>
                                                                                <option value="Admin">Admin</option>
                                                                                <option value="Credit Score">Credit Score</option>
                                                                                <option value="Financial Institution">Financial Institution</option>
                                                                            </select>
                                                                        </fieldset>

                                                                        <fieldset class="form-group position-relative has-icon-left">
                                                                            <input type="password" class="form-control form-control-lg input-lg" id="user-password"
                                                                                   placeholder="Enter Password" name="password"  required>
                                                                            <div class="form-control-position">
                                                                                <i class="la la-key"></i>
                                                                            </div>
                                                                        </fieldset>

                                                                        <button type="submit" class="btn btn-login btn-lg btn-block"><i class="ft-unlock"></i> Update</button>
                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><a href="{{ route('backend.DeleteAccount',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
@endsection
