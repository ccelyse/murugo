<div class="sf-pushnav quarter-size">
    <div class="sf-pushnav-wrapper container">
        <a href="{{url('/')}}" class="sf-pushnav-trigger sf-pushnav-close">
            <svg version="1.1" id="sf-pushnav-close" class="sf-hover-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="48px" height="48px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve">
                  <path fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                     M24,2c12.15,0,22,9.85,22,22s-9.85,22-22,22S2,36.15,2,24C2,11.902,11.766,2.084,23.844,2" />
                <path class="cross" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" d="M16,32l16-16 M32,32L16,16
                     M16,32l16-16" />
               </svg>
        </a>
        <div class="sf-pushnav-menu col-sm-6 col-sm-offset-1">
            <nav class="clearfix">
                <div class="menu-mobile-reduced-container">
                    <ul id="mega-menu-main_navigation" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="disabled" data-second-click="close" data-document-click="collapse" data-reverse-mobile-items="true" data-vertical-behaviour="standard" data-breakpoint="600">
                        <li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-current-menu-ancestor mega-current-menu-parent mega-current_page_parent mega-current_page_ancestor mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14712 no-headings ' id='mega-menu-item-14712'>
                            <a class="mega-menu-link" href="{{url('/')}}">Home</a>
                        </li>
                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-15734 no-headings ' id='mega-menu-item-15734'>
                            <a class="sf-icon-filter mega-menu-link" href="{{url('/AboutUs')}}">About Us</a>
                        </li>
                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14608 no-headings ' id='mega-menu-item-14608'>
                            <a class="mega-menu-link">Members</a>
                            <ul class="mega-sub-menu" style="display: none;">
                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/MemberDirectory')}}">Member Directory</a></li>
                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/BecomeMember')}}">Become a member</a></li>
                            </ul>
                        </li>

                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-14646 no-headings ' id='mega-menu-item-14646'>
                            <a class="mega-menu-link" href="{{url('/BusinessLicensing')}}">BUSINESS LICENSING</a>
                        </li>
                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14608 no-headings ' id='mega-menu-item-14608'>
                            <a class="mega-menu-link">RESOURCES</a>
                            <ul class="mega-sub-menu" style="display: none;">
                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/News')}}">News</a></li>
                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/Publications')}}">Publications</a></li>
                            </ul>
                        </li>

                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-14644 no-headings ' id='mega-menu-item-14644'>
                            <a class="mega-menu-link" href="{{url('/BecomeMember')}}">Join Us</a>
                        </li>


                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14608 no-headings ' id='mega-menu-item-14608'>
                            <a class="mega-menu-link">Log In</a>
                            <ul class="mega-sub-menu" style="display: none;">
                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/Webmail')}}">Webmail</a></li>
                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/login')}}">Portal</a></li>
                            </ul>
                        </li>
                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14479 no-headings ' id='mega-menu-item-14479'>
                            <a class="mega-menu-link" href="{{url('/ContactUs')}}">Contact Us</a>
                        </li>

                    </ul>

                </div>
            </nav>
        </div>

    </div>
</div>
<div id="container">
    <div id="mobile-menu-wrap" class="menu-is-left">
        <nav id="mobile-menu" class="clearfix">
            <div class="menu-main-menu-container">
                <ul id="mega-menu-main_navigation" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="disabled" data-second-click="close" data-document-click="collapse" data-reverse-mobile-items="true" data-vertical-behaviour="standard" data-breakpoint="600">
                    <li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-current-menu-ancestor mega-current-menu-parent mega-current_page_parent mega-current_page_ancestor mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14712 no-headings ' id='mega-menu-item-14712'>
                        <a class="mega-menu-link" href="{{url('/')}}">Home</a>
                    </li>
                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-15734 no-headings ' id='mega-menu-item-15734'>
                        <a class="sf-icon-filter mega-menu-link" href="{{url('/AboutUs')}}">About Us</a>
                    </li>
                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14608 no-headings ' id='mega-menu-item-14608'>
                        <a class="mega-menu-link">Members</a>
                        <ul class="mega-sub-menu" style="display: none;">
                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/MemberDirectory')}}">Members Directory</a></li>
                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/BecomeMember')}}">Become a member</a></li>
                        </ul>
                    </li>

                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-14646 no-headings ' id='mega-menu-item-14646'>
                        <a class="mega-menu-link" href="{{url('/BusinessLicensing')}}">BUSINESS LICENSING</a>
                    </li>
                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14608 no-headings ' id='mega-menu-item-14608'>
                        <a class="mega-menu-link">RESOURCES</a>
                        <ul class="mega-sub-menu" style="display: none;">
                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/News')}}">News</a></li>
                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/Publications')}}">Publications</a></li>
                        </ul>
                    </li>

                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-14644 no-headings ' id='mega-menu-item-14644'>
                        <a class="mega-menu-link" href="{{url('/BecomeMember')}}">Join Us</a>
                    </li>


                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14608 no-headings ' id='mega-menu-item-14608'>
                        <a class="mega-menu-link">Log In</a>
                        <ul class="mega-sub-menu" style="display: none;">
                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/Webmail')}}">Webmail</a></li>
                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/login')}}">Portal</a></li>
                        </ul>
                    </li>
                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14479 no-headings ' id='mega-menu-item-14479'>
                        <a class="mega-menu-link" href="{{url('/ContactUs')}}">Contact Us</a>
                    </li>

                </ul>

            </div>

        </nav>

    </div>
    <header id="mobile-header" class="mobile-center-logo clearfix">
        <div class="mobile-header-opts opts-left"><button class="hamburger mobile-menu-link hamburger--3dy" type="button">
               <span class="hamburger-box">
               <span class="hamburger-inner"></span>
               </span>
            </button>
        </div>
        <div id="mobile-logo" class="logo-center has-light-logo has-img clearfix" data-anim="tada">
            <a href="{{url('/')}}">
                <img class="standard" src="images/RHAlogo.png" alt="RHA" height="50" width="30" />
                <img class="retina" src="images/RHAlogo.png" alt="RHA" height="50" width="30" />
                <div class="text-logo"></div>
            </a>
        </div>

    </header>
    <div class="header-wrap  full-center full-header-stick page-header-naked-light" data-style="light" data-default-style="light">
        <div id="header-section" class="header-3 ">
            <header id="header" class="sticky-header clearfix">

                <div class="container">
                    <div class="row">
                        <div id="logo" class="col-sm-4 logo-left has-light-logo has-img clearfix" data-anim="tada">
                            <a href="{{url('/')}}">
                                <img class="standard" src="images/RHAlogo.png" alt="RHA" height="30" width="77" />
                                <img class="retina" src="images/RHAlogo.png" alt="RHA" height="30" width="77" />
                                <img class="light-logo" src="images/RHAlogo.png" alt="RHA" height="30" width="77" />
                                <div class="text-logo"></div>
                            </a>
                        </div>
                        <div class="float-menu">
                            <nav id="main-navigation" class="std-menu clearfix pull-right">
                                <div id="mega-menu-wrap-main_navigation" class="mega-menu-wrap">
                                    <div class="mega-menu-toggle">
                                        <div class='mega-toggle-block mega-menu-toggle-block mega-toggle-block-right' id='mega-toggle-block-1'></div>
                                    </div>
                                    <ul id="mega-menu-main_navigation" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="disabled" data-second-click="close" data-document-click="collapse" data-reverse-mobile-items="true" data-vertical-behaviour="standard" data-breakpoint="600">
                                        <li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-current-menu-ancestor mega-current-menu-parent mega-current_page_parent mega-current_page_ancestor mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14712 no-headings ' id='mega-menu-item-14712'>
                                            <a class="mega-menu-link" href="{{url('/')}}">Home</a>
                                        </li>
                                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-15734 no-headings ' id='mega-menu-item-15734'>
                                            <a class="sf-icon-filter mega-menu-link" href="{{url('/AboutUs')}}">About Us</a>
                                        </li>
                                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14608 no-headings ' id='mega-menu-item-14608'>
                                            <a class="mega-menu-link">Members</a>
                                            <ul class="mega-sub-menu" style="display: none;">
                                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/MemberDirectory')}}">Member Directory</a></li>
                                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/BecomeMember')}}">Become a member</a></li>
                                            </ul>
                                        </li>

                                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-14646 no-headings ' id='mega-menu-item-14646'>
                                            <a class="mega-menu-link" href="{{url('/BusinessLicensing')}}">BUSINESS LICENSING</a>
                                        </li>
                                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14608 no-headings ' id='mega-menu-item-14608'>
                                            <a class="mega-menu-link">RESOURCES</a>
                                            <ul class="mega-sub-menu" style="display: none;">
                                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/News')}}">News</a></li>
                                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/Publications')}}">Publications</a></li>
                                            </ul>
                                        </li>

                                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-14644 no-headings ' id='mega-menu-item-14644'>
                                            <a class="mega-menu-link" href="{{url('/BecomeMember')}}">Join Us</a>
                                        </li>


                                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14608 no-headings ' id='mega-menu-item-14608'>
                                            <a class="mega-menu-link">Log In</a>
                                            <ul class="mega-sub-menu" style="display: none;">
                                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/Webmail')}}">Webmail</a></li>
                                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-menu-item mega-page_item mega-page-item-9 mega-current_page_item mega-menu-item-14680  " id="mega-menu-item-14680"><a class="mega-menu-link" href="{{url('/login')}}">Portal</a></li>
                                            </ul>
                                        </li>
                                        <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-14479 no-headings ' id='mega-menu-item-14479'>
                                            <a class="mega-menu-link" href="{{url('/ContactUs')}}">Contact Us</a>
                                        </li>

                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    </div>
