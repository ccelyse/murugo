<div id="footer-wrap">
    <footer id="footer" class="">
        <div class="container">
            <div id="footer-widgets" class="row clearfix">
                <div class="col-md-12">
                    <section id="text-3" class="widget widget_text clearfix">
                        <div class="textwidget">
                            <p>©2018 RHA - Crafted By <a href="http://www.sd.rw" style="color:#fff;">Skyline Digital</a></p>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>

<div id="back-to-top" class="animate-top"><i class="sf-icon-up-chevron"></i></div>
<div class="fw-video-area">
    <div class="fw-video-close"><i class="sf-icon-remove"></i></div>
    <div class="fw-video-wrap"></div>
</div>


<div class="fw-video-spacer"></div>

<div id="sf-included" class="has-products has-carousel has-parallax has-team stickysidebars "></div>

<div class="sf-svg-loader"><object data="images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object></div>

<div id="loveit-locale" data-ajaxurl="http://uplift.swiftideas.com/wp-admin/admin-ajax.php" data-nonce="2e85738f5b" data-alreadyloved="You have already loved this item." data-error="Sorry, there was a problem processing your request." data-loggedin="false"></div>
<script data-cfasync="false" src="cdn-cgi/scripts/f2bf09f8/cloudflare-static/email-decode.min.js"></script><script type="text/javascript">
    setTimeout(function(){var a=document.createElement("script");
        var b=document.getElementsByTagName('script')[0];
        a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0049/1794.js";
        a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<div class="sf-container-overlay">
    <div class="sf-loader">
        <div class="sf-svg-loader"><object data="images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object></div>
    </div>
</div>
<script type='text/javascript' src='js/jquery/ui/core.min-ver=1.11.4.js'></script>
<script type='text/javascript' src='js/jquery/ui/widget.min-ver=1.11.4.js'></script>
<script type='text/javascript' src='js/jquery/ui/button.min-ver=1.11.4.js'></script>
<script type='text/javascript' src='js/jquery/ui/spinner.min-ver=1.11.4.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var jckqv = {"ajaxurl":"http:\/\/uplift.swiftideas.com\/wp-admin\/admin-ajax.php","nonce":"6ed2845eef","settings":{"styling_autohide":"0","styling_hoverel":".product","styling_icon":"eye","styling_text":"Quickview","styling_btnstyle":"flat","styling_padding":["8","10","8","10"],"styling_btncolour":"#66cc99","styling_btnhovcolour":"#47C285","styling_btntextcolour":"#ffffff","styling_btntexthovcolour":"#ffffff","styling_borderradius":["4","4","4","4"],"position_autoinsert":"1","position_position":"afteritem","position_align":"left","position_margins":["0","0","10","0"],"general_method":"click","imagery_imgtransition":"horizontal","imagery_transitionspeed":"600","imagery_autoplay":"0","imagery_autoplayspeed":"3000","imagery_infinite":"1","imagery_navarr":"1","imagery_thumbnails":"thumbnails","content_showtitle":"1","content_showprice":"1","content_showrating":"1","content_showbanner":"1","content_showdesc":"short","content_showatc":"1","content_ajaxcart":"1","content_autohidepopup":"1","content_showqty":"1","content_showmeta":"1","content_themebtn":"0","content_btncolour":"#66cc99","content_btnhovcolour":"#47C285","content_btntextcolour":"#ffffff","content_btntexthovcolour":"#ffffff","general_gallery":"1","general_overlaycolour":"#000000","general_overlayopacity":"0.8"},"imgsizes":{"catalog":{"width":"700","height":"791","crop":1},"single":{"width":"700","height":"791","crop":1},"thumbnail":{"width":"120","height":"136","crop":1}},"url":"http:\/\/uplift.swiftideas.com","text":{"added":"Added!","adding":"Adding to Cart...","loading":"Loading..."}};
    /* ]]> */
</script>
<!--<script type='text/javascript' src='wp-content/plugins/jck-woo-quickview/assets/frontend/js/main.min-ver=4.9.6.js'></script>-->
<!--<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/jquery.form.min-ver=3.51.0-2014.06.20.js'></script>-->
<!--<script type='text/javascript'>-->
<!--    /* <![CDATA[ */-->
<!--    var _wpcf7 = {"loaderUrl":"http:\/\/uplift.swiftideas.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptchaEmpty":"Please verify that you are not a robot.","sending":"Sending ...","cached":"1"};-->
<!--    /* ]]> */-->
<!--</script>-->
<!--<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts-ver=4.4.2.js'></script>-->
<script type='text/javascript' src='swift-framework/includes/page-builder/frontend-assets/js/spb-functions.js'></script>
<script type='text/javascript' data-cfasync="true" src='swift-framework/includes/swift-slider/assets/js/swift-slider.js'></script>
<!--<script type='text/javascript'>-->
<!--    /* <![CDATA[ */-->
<!--    var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View Cart","cart_url":"http:\/\/uplift.swiftideas.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};-->
<!--    /* ]]> */-->
<!--</script>-->
<!--<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min-ver=2.5.5.js'></script>-->
<!--<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min-ver=2.70.js'></script>-->
<!--<script type='text/javascript'>-->
<!--    /* <![CDATA[ */-->
<!--    var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};-->
<!--    /* ]]> */-->
<!--</script>-->
<!--<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min-ver=2.5.5.js'></script>-->
<!--<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min-ver=1.4.1.js'></script>-->
<!--<script type='text/javascript'>-->
<!--    /* <![CDATA[ */-->
<!--    var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments"};-->
<!--    /* ]]> */-->
<!--</script>-->
<!--<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min-ver=2.5.5.js'></script>-->
<!--<script type='text/javascript' src='wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min-ver=1.2.0.js'></script>-->
<!--<script type='text/javascript'>-->
<!--    /* <![CDATA[ */-->
<!--    var yith_wcwl_l10n = {"ajax_url":"\/wp-admin\/admin-ajax.php","redirect_to_cart":"no","multi_wishlist":"","hide_add_button":"1","is_user_logged_in":"","ajax_loader_url":"http:\/\/uplift.swiftideas.com\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif","remove_from_wishlist_after_add_to_cart":"yes","labels":{"cookie_disabled":"We are sorry, but this feature is available only if cookies are enabled on your browser.","added_to_cart_message":"<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"},"actions":{"add_to_wishlist_action":"add_to_wishlist","remove_from_wishlist_action":"remove_from_wishlist","move_to_another_wishlist_action":"move_to_another_wishlsit","reload_wishlist_and_adding_elem_action":"reload_wishlist_and_adding_elem"}};-->
<!--    /* ]]> */-->
<!--</script>-->
<script type='text/javascript' src='js/jquery.yith-wcwl-ver=2.0.15.js'></script>
<script type='text/javascript' src='js/combine/bootstrap.min.js'></script>
<script type='text/javascript' src='js/combine/jquery-ui-1.10.2.custom.min.js'></script>
<script type='text/javascript' src='js/combine/owl.carousel.min.js'></script>
<script type='text/javascript' src='js/combine/theme-scripts.min.js'></script>
<script type='text/javascript' src='js/combine/ilightbox.min.js'></script>
<script type='text/javascript' src='js/combine/jquery.isotope.min.js'></script>
<script type='text/javascript' src='js/combine/imagesloaded.js'></script>
<script type='text/javascript' src='js/combine/jquery.infinitescroll.min.js'></script>
<script type='text/javascript' src='js/functions.js'></script>
<script type='text/javascript' src='js/hoverIntent.min-ver=1.8.1.js'></script>
<script type='text/javascript' src='js/wp-embed.min-ver=4.9.6.js'></script>
</body>
</html>