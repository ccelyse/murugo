<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('backend.Login');
//});


Route::get('/',['as'=>'backend.SignIn','uses'=>'BackendController@SignIn']);
//Route::post('/SignIn', ['as' => 'SignIn', 'uses' => 'BackendController@SignIn']);
Route::post('/SignIn_', ['as' => 'SignIn_', 'uses' => 'BackendController@SignIn_']);
Route::get('/TestEmail',['as'=>'TestEmail','uses'=>'BackendController@TestEmail']);

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => 'disablepreventback'],function(){
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/Dashboard', ['as' => 'backend.Dashboard', 'uses' => 'BackendController@Dashboard']);
        Route::get('/CDashboard', ['as' => 'backend.CDashboard', 'uses' => 'BackendController@CDashboard']);
        Route::get('/FDashboard', ['as' => 'backend.FDashboard', 'uses' => 'BackendController@FDashboard']);
        Route::get('/Property', ['as' => 'backend.Property', 'uses' => 'BackendController@Property']);
        Route::get('/PropertyRangeB', ['as' => 'backend.PropertyRangeB', 'uses' => 'BackendController@PropertyRangeB']);
        Route::post('/AddPropertyRangeB', ['as' => 'backend.AddPropertyRangeB', 'uses' => 'BackendController@AddPropertyRangeB']);
        Route::post('/UpdatePropertyRangeB', ['as' => 'backend.UpdatePropertyRangeB', 'uses' => 'BackendController@UpdatePropertyRangeB']);
        Route::get('/DeletePropertyRangeB', ['as' => 'backend.DeletePropertyRangeB', 'uses' => 'BackendController@DeletePropertyRangeB']);
        Route::post('/AddProperty', ['as' => 'backend.AddProperty', 'uses' => 'BackendController@AddProperty']);
        Route::post('/EditProperty', ['as' => 'backend.EditProperty', 'uses' => 'BackendController@EditProperty']);
        Route::get('/DeleteProperty', ['as' => 'backend.DeleteProperty', 'uses' => 'BackendController@DeleteProperty']);
        Route::get('/Developer', ['as' => 'backend.developer', 'uses' => 'BackendController@developer']);
        Route::post('/AddDeveloper', ['as' => 'backend.AddDeveloper', 'uses' => 'BackendController@AddDeveloper']);
        Route::post('/EditDeveloper', ['as' => 'backend.EditDeveloper', 'uses' => 'BackendController@EditDeveloper']);
        Route::get('/DeleteDeveloper', ['as' => 'backend.DeleteDeveloper', 'uses' => 'BackendController@DeleteDeveloper']);
        Route::get('/Customers', ['as' => 'backend.Customers', 'uses' => 'BackendController@Customers']);
        Route::get('/ApplicationList', ['as' => 'backend.ApplicationList', 'uses' => 'BackendController@ApplicationList']);

        Route::get('/AccountList', ['as' => 'backend.AccountList', 'uses' => 'BackendController@AccountList']);
        Route::get('/CreateAccount', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount']);
        Route::post('/CreateAccount_', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount_']);
        Route::get('/DeleteAccount', ['as' => 'backend.DeleteAccount', 'uses' => 'BackendController@DeleteAccount']);
        Route::post('/EditAccount', ['as' => 'backend.EditAccount', 'uses' => 'BackendController@EditAccount']);

        Route::get('/SendSMS', ['as' => 'backend.SendSMS', 'uses' => 'BackendController@SendSMS']);
        Route::get('/ApproveSendSMS', ['as' => 'backend.ApproveSendSMS', 'uses' => 'BackendController@ApproveSendSMS']);
        Route::get('/RejectSendSMS', ['as' => 'backend.RejectSendSMS', 'uses' => 'BackendController@RejectSendSMS']);

        Route::get('/CreditScore', ['as' => 'backend.CreditScore', 'uses' => 'BackendController@CreditScore']);
        Route::post('/CreditScoreComment', ['as' => 'backend.CreditScoreComment', 'uses' => 'BackendController@CreditScoreComment']);

        Route::get('/FinancialInstitution', ['as' => 'backend.FinancialInstitution', 'uses' => 'BackendController@FinancialInstitution']);



        Route::get('/SendSMS_', ['as' => 'backend.SendSMS_', 'uses' => 'BackendController@SendSMS_']);
        Route::get('/SendEmail', ['as' => 'backend.SendEmail', 'uses' => 'BackendController@SendEmail']);
        Route::post('/SendEmail_', ['as' => 'backend.SendEmail_', 'uses' => 'BackendController@SendEmail_']);

    });
});





