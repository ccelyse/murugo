<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/MoreAttractionAPI', ['as'=>'MoreAttractionAPI','uses'=>'FrontendController@MoreAttractionAPI']);
Route::get('/PropertyMobile', ['as'=>'PropertyMobile','uses'=>'BackendController@PropertyMobile']);
Route::post('/PropertyRange', ['as'=>'PropertyRange','uses'=>'BackendController@PropertyRange']);
Route::get('/PropertyRangeApi', ['as'=>'PropertyRangeApi','uses'=>'BackendController@PropertyRangeApi']);
Route::post('/PropertyMore', ['as'=>'PropertyMore','uses'=>'BackendController@PropertyMore']);
Route::post('/PropertyGallery', ['as'=>'PropertyGallery','uses'=>'BackendController@PropertyGallery']);
Route::post('/CustomerSignUp', ['as'=>'CustomerSignUp','uses'=>'BackendController@CustomerSignUp']);
Route::post('/CustomerSignIn', ['as'=>'CustomerSignIn','uses'=>'BackendController@CustomerSignIn']);
Route::post('/CustomerDetails', ['as'=>'CustomerDetails','uses'=>'BackendController@CustomerDetails']);
Route::post('/CustomerInformation', ['as'=>'CustomerInformation','uses'=>'BackendController@CustomerInformation']);
Route::post('/UpdateCustomerInformation', ['as'=>'UpdateCustomerInformation','uses'=>'BackendController@UpdateCustomerInformation']);
Route::post('/ApplyMortgage', ['as'=>'ApplyMortgage','uses'=>'BackendController@ApplyMortgage']);
Route::post('/LevelOneTracker', ['as'=>'LevelOneTracker','uses'=>'BackendController@LevelOneTracker']);
Route::post('/LevelTwoTracker', ['as'=>'LevelTwoTracker','uses'=>'BackendController@LevelTwoTracker']);
Route::post('/LevelTracker', ['as'=>'LevelTracker','uses'=>'BackendController@LevelTracker']);
//Route::get('/AttractionAPI', ['as' => 'AttractionAPI', 'uses' => 'FrontendController@AttractionAPI']);